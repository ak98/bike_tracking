# Bike_Tracking

## Project goal
A final thesis report for graduation in AP degree of IT Technology, spring 2020.

The repository does not include all materials, files nor the final report itself because of the personal reasons. The repository showcases mainly thinking process behind the project([flowchart](../master/flowchart.jpg), [system block diagram](../master/Bike_tracker.png)), documentation along the way(issues, [code](../master/code/)), ability to manage the project(milestones, issues).

## The problem statement
The project 's main goal is development of bike tracker prototype based on the requirements specified in the product description.

The goals/tasks within the prototype development include:

*	product definition as a template for prototype design
*	prototype design, features description, hardware selection
*	testing and evaluation of the prototype
*	suggestion for further development based on the current development experience

The personal goals during this project include:

*	learning about new technologies within hardware and software
*	improving project management skills
*	reviewing the concepts learned during the education

## Conclusion
Despite of not fulfilling the main goal of the project, the sub goals of the project were fulfilled.
The project succeeded in defining the product, which served as a guideline for the prototype design. The prototype design was created with particular hardware in regards to the features description. The testing on the system was conducted with documentation of the results. The results were evaluated and suggestions for further development were made for the relevant  parts of the system.

Furthermore, the project served successfully as a review of the concepts learned during the education.  New knowledge was gained regarding the hardware and software. Project management skills were put in the practice and improved during the project period. 


