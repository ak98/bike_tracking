# This file is executed on every boot (including wake-boot from deepsleep)
#import esp
#esp.osdebug(None)
import uos, machine
uos.dupterm(None, 1) # disable REPL on UART(0)
import gc
import webrepl
webrepl.start()
import sensor
import uart
import gps
import app
import sim
import functions
gc.collect()



