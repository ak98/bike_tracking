from machine import Pin
import gps
import app
import functions

sensor_data = Pin(functions.get_conf("sensor_output_pin"), Pin.IN)
sensor_power = Pin(functions.get_conf("sensor_power_pin"), Pin.OUT)


def read(pin=sensor_data):
    return pin.value()


def on():
    sensor_power.on()
    return True


def off():
    sensor_power.off()
    return False


def callback(pin):
    if read(Pin(functions.get_conf("gps_power_pin"))) == 0:
        gps.on()
        off()
        print("Sensor active")
        app.Movement.sensor_active = True


sensor_data.irq(trigger=Pin.IRQ_RISING | Pin.IRQ_FALLING, handler=callback)
