import json


def read_conf():
    with open("conf.txt", "r") as file:
        config = json.load(file)
    return config

def write_conf(config):
    with open("conf.txt", "w") as file:
        json.dump(config, file)

def get_conf(key):
    return read_conf()[key]

def change_mode(mode):
    config = read_conf()
    config["mode"] = mode
    write_conf(config)

def remove_file(file="log.txt"):
    import os
    os.remove(file)