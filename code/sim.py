import uart
import json
import functions


def validate_response():
    data = b''
    while True:
        if uart.uart.any():
            data += uart.uart.readline()
            if b"ERROR" in data:
                print(data)
                return False
            if b"OK" in data or b"DOWNLOAD" in data or b">" in data:
                print("OK", data)
                return data


def validate_http_response():
    data = b''
    import time
    start_time = time.time()
    while time.time() - start_time < 10:
        if uart.uart.any():
            data += uart.uart.readline()
            if b",200" in data and b"HTTPACTION:" in data:
                print("OK", data)
                return data
    return False


def send_sms(text, number=functions.get_conf("number_owner")):
    uart.clear_buf()
    uart.change_rxbuf_size(80)
    uart.uart.write("AT+CMGS=\""+ number + "\"\r")
    validate_response()
    uart.uart.write(text + "\x1a")
    validate_response()

def delete_read_sms():
    uart.clear_buf()
    uart.uart.write("AT+CMGDA=\"DEL READ\"\r")
    validate_response()

def get_read_sms():
    uart.clear_buf()
    uart.change_rxbuf_size(128)
    uart.uart.write("AT+CMGL=\"REC READ\"\r")
    validate_response()

def get_unread_sms():
    uart.clear_buf()
    uart.change_rxbuf_size(128)
    uart.uart.write("AT+CMGL=\"REC UNREAD\"\r")
    return validate_response().decode("utf-8")


def evaluate_command():
    message = get_unread_sms()

    if "+CMGL:" not in message:
        print("No message found")
        return False
    if "+421" not in message: # subscription bypass
        print("Unauthorized user")
        send_sms("Unauthorized user.\r\nMessage:\r\n" + message)
        return False

    if "GUARD" in message:
        print("Tracker initiating GUARD mode")
        functions.change_mode("GUARD")
        return True
    elif "READY" in message:
        print("Back to READY mode")
        functions.change_mode("READY")
        return True
    else:
        print("Invalid command")
        send_sms("Invalid command.\r\nOptions are: GUARD, READY")
        return False


class Http:

    @staticmethod
    def request_context(apn, url):
        uart.uart.write('AT+SAPBR=3,1,"Contype","GPRS"\r')
        validate_response()
        uart.uart.write('AT+SAPBR=3,1,"APN",\"' + apn + '\"\r')
        validate_response()
        uart.uart.write('AT+SAPBR=1,1\r')
        validate_response()
        uart.uart.write('AT+HTTPINIT\r')
        validate_response()
        uart.uart.write('AT+HTTPPARA="CID",1\r')
        validate_response()
        uart.uart.write('AT+HTTPPARA="URL",\"' + url + '\"\r')
        validate_response()
        uart.uart.write('AT+HTTPSSL=1\r')
        validate_response()

    @staticmethod
    def extract_json():
        uart.uart.write('AT+HTTPREAD\r')
        response = validate_response().decode("utf-8")
        uart.uart.write('AT+HTTPTERM\r')
        validate_response()
        uart.uart.write('AT+SAPBR=0,1\r')
        validate_response()
        string = response[response.index("{"):response.rindex("}") + 1]
        data = json.loads(string)
        return data

    @staticmethod
    def get(apn=functions.get_conf("apn"), url=functions.get_conf("get_url")):
        uart.clear_buf()
        uart.change_rxbuf_size(612)
        Http.request_context(apn, url)
        uart.uart.write('AT+HTTPACTION=0\r')
        if validate_http_response():
            return Http.extract_json()

        return False

    @staticmethod
    def post(apn=functions.get_conf("apn"), url=functions.get_conf("post_url")):
        uart.clear_buf()
        uart.change_rxbuf_size(612)
        payload = "{\"name\": \"morpheus\", \"job\": \"leader\"}"
        Http.request_context(apn, url)
        uart.uart.write('AT+HTTPPARA="CONTENT","application/json"\r')
        validate_response()
        uart.uart.write('AT+HTTPDATA=55,2000\r')
        validate_response()
        uart.uart.write(payload)
        validate_response()
        uart.uart.write('AT+HTTPACTION=1\r')
        if validate_http_response():
            return Http.extract_json()

        return False
