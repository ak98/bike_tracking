import uart
import functions
from machine import Pin

gps_power = Pin(functions.get_conf("gps_power_pin"), Pin.OUT)

messages = {"speed_on": b'\xB5\x62\x06\x01\x03\x00\xF0\x05\x01\x00\x1A',
            "speed_off": b'\xB5\x62\x06\x01\x03\x00\xF0\x05\x00\xFF\x19',
            "location_on": b'\xB5\x62\x06\x01\x03\x00\xF0\x01\x01\xFC\x12',
            "location_off": b'\xB5\x62\x06\x01\x03\x00\xF0\x01\x00\xFB\x11',
            "save": b'\xB5\x62\x06\x09\x0D\x00\x00\x00\x00\x00\xFF\xFF\x00\x00\x00\x00\x00\x00\x03\x1D\xAB'}


def set_mode(mode):

    if mode == "MONITOR":
        uart.uart.write(messages["speed_on"])
        uart.uart.write(messages["location_off"])
        uart.change_rxbuf_size(35)
    elif mode == "CHASE":
        uart.uart.write(messages["location_on"])
        uart.uart.write(messages["speed_off"])
        uart.change_rxbuf_size(52)

    uart.uart.write(messages["save"])
    uart.wait_for_char("@")
    return mode


def validate_data():
    data = uart.read_line()
    if not data:
        return False

    if "\r\n" not in data:
        return False

    if "$" not in data:
        return False

    return data


def valid_fix(string):
    if string:
        return "A" in string
    return False

def is_bike_moving(data):
    if float(data[21:26]) > 7:
        # print("Bike's moving unexpectedly")
        # SMS to the owner
        set_mode("CHASE")
        return True

    return False


def on():
    print("GPS is on")
    gps_power.on()
    return True


def off():
    print("GPS is off")
    gps_power.off()
    return False
