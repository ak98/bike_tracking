import gps
import sensor


class Movement:
    sensor_active = False
    sensor_activation_time = None
    chase = False


def run():
    gps.off()
    sensor.on()
    import time
    start = time.time()
    while time.time() - start < 140:
        if not Movement.sensor_active and not Movement.chase:
            pass
        elif Movement.sensor_active and not Movement.chase:
            Movement.sensor_activation_time = time.time()
            gps.set_mode("MONITOR")
            time.sleep(1)
            while time.time() - start < 120:  # 2 minutes
                data = gps.validate_data()
                time.sleep(1)
                if gps.valid_fix(data):

                    if gps.is_bike_moving(data):
                        print("Panic")
                        Movement.chase = True
                        # panic, bike was stolen
            if not Movement.chase:
                gps.off()
                Movement.sensor_active = False
                sensor.on()  # optional
                print("Bike is not moving")
            with open("log.txt", "a+") as file:
                file.write(data)
        elif Movement.chase:
            pass