from machine import UART
from time import sleep

uart = UART(0, 9600)
uart.init(9600, bits=8, parity=None, stop=1, rxbuf=35)


def read_line():
    if uart.any():
        try:
            return uart.readline().decode("utf-8")
        except UnicodeError:
            sleep(0.5)
            try:
                return uart.readline().decode("utf-8")
            except AttributeError:
                sleep(0.5)
                return uart.readline().decode("utf-8")

    return False

def change_rxbuf_size(size):
    uart.init(9600, bits=8, parity=None, stop=1, rxbuf=size) # 35 speed, 52 location


def clear_buf():
    uart.read()


def wait_for_char(char):
    data = b""
    while char not in data:
        if uart.any():
            data += uart.read()